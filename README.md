# RealTime Chat App with Auth

## Technologies:
[![My Skills](https://skillicons.dev/icons?i=nextjs,go)](https://skillicons.dev)
**WebSocket**

## Features

### 1. Auth -> Room Creation
![Demo GIF](assets/demo1.gif)


### 2. Room Joining -> RealTime Chat 
![Demo GIF](assets/demo2.gif)


## Local Development Instructions

### Setting up the Server

```
cd server
go run cmd/main.go
```
`API_URL=http://localhost:8080`
`WEBSOCKET_URL=ws://localhost:8080`
### Setting up the Client

```
cd client
npm install
npm run dev
```
`CLIENT_URL=http://localhost:3000`

---
Originial Link: https://github.com/dhij/go-next-ts_chat
Icon Link: https://github.com/tandpfun/skill-icons
GitHub Link: https://github.com/prajjwalyd
Twitter: https://twitter.com/prajjwalyd

---

## Feel Free to Suggest Changes/Improvements by Raising Issues